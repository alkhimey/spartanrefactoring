package il.ac.technion.cs.ssdl.spartan.refactoring;


import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Assignment.Operator;
import org.eclipse.jdt.core.dom.ASTMatcher;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ConditionalExpression;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;

public class ConvertToTernaryRefactoring extends BaseRefactoring {	
	@Override
	public String getName() {
		return "Convert Conditional Into a Trenary";
	}
	
	@Override
	protected ASTRewrite createRewrite(CompilationUnit cu, SubProgressMonitor pm) {
		pm.beginTask("Creating rewrite operation...", 1);
		
		final AST ast = cu.getAST();
		final ASTRewrite rewrite = ASTRewrite.create(ast);
		
		cu.accept(new ASTVisitor() {
			public boolean visit(IfStatement node) {
				if (isNodeOutsideSelection(node))
					return true;
				
				if (node.getElseStatement() == null)
					return true;
				
				ASTNode newnode = handleAssignment(ast, rewrite, node);
				if(newnode == null)
					newnode = handleReturn(ast, rewrite, node);
				if(newnode == null)
					return true;
				
				rewrite.replace(node, newnode, null);						
				return false; // TODO: Really?
			}
		});
		
		pm.done();
		return rewrite;
	}	

	/**
	 * Extracts an assignment from a node. 
	 * @param node The node from which to extract assignment.
	 * @return null if it is not possible to extract the assignment.
	 */
	Assignment getAssignment(Statement node) {
		ExpressionStatement expStmnt = null;
		
		if(node.getNodeType() == ASTNode.EXPRESSION_STATEMENT) {
			expStmnt = (ExpressionStatement)node;
		} else if (!(node.getNodeType() == ASTNode.BLOCK))
			return null;
		else {
			Block block = (Block)node;
		
			if(block.statements().size() != 1)
				return null;
		
			if(((ASTNode)block.statements().get(0)).getNodeType() != ASTNode.EXPRESSION_STATEMENT)
				return null;
			
			expStmnt = (ExpressionStatement)((ASTNode)block.statements().get(0));
		
		}
		
		if(expStmnt.getExpression().getNodeType() != ASTNode.ASSIGNMENT)
			return null;
	
		return (Assignment)expStmnt.getExpression();
	}

	/**
	 * Extracts a return statement from a node. 
	 * Expression, and the Expression contains Assignment.
	 * @param node The node from which to return statement assignment.
	 * @return null if it is not possible to extract the return statement.
	 */
	ReturnStatement getReturnStatement(Statement node) {
		if(node.getNodeType() == ASTNode.RETURN_STATEMENT) {
			return (ReturnStatement)node;
		} else if (!(node.getNodeType() == ASTNode.BLOCK))
			return null;
		else {
			Block block = (Block)node;
		
			if(block.statements().size() != 1)
				return null;
		
			if(((ASTNode)block.statements().get(0)).getNodeType() != ASTNode.RETURN_STATEMENT)
				return null;
			
			return ((ReturnStatement)block.statements().get(0));	
		}	
	}
	
	
	/**
	 * If possible rewrite the if statement as return of a ternary operation.
	 * @param node The root if node.
	 * @return Returns null if it is not possible to rewrite as return. Otherwise returns the new node.
	 */
	private ReturnStatement handleReturn(AST ast, ASTRewrite rewrite,
			IfStatement node) {
		
		ReturnStatement retThen = getReturnStatement(node.getThenStatement());
		ReturnStatement retElse = getReturnStatement(node.getElseStatement());
		
		if(retThen == null || retElse == null)
			return null;
		
		ConditionalExpression newCondExp = ast.newConditionalExpression();
		newCondExp.setExpression((Expression) rewrite.createMoveTarget(node.getExpression()));
		newCondExp.setThenExpression((Expression) rewrite.createMoveTarget(retThen.getExpression()));
		newCondExp.setElseExpression((Expression) rewrite.createMoveTarget(retElse.getExpression()));
		
		ReturnStatement newnode = ast.newReturnStatement();
		newnode.setExpression(newCondExp);
		
		return newnode;
	}
	
	
	
	
	/**
	 * If possible rewrite the if statement as assignment of a ternary operation.
	 * @param node The root if node.
	 * @return Returns null if it is not possible to rewrite as assignment. Otherwise returns the new node.
	 */
	private ExpressionStatement handleAssignment(final AST ast,
			final ASTRewrite rewrite, IfStatement node) {
		Assignment asgnThen = getAssignment(node.getThenStatement());
		Assignment asgnElse = getAssignment(node.getElseStatement());

		if(asgnThen == null || asgnElse == null) 
			return null;
		
		/*if(((SimpleName)assThen.getLeftHandSide()).isDeclaration() || ((SimpleName)assElse.getLeftHandSide()).isDeclaration())
			return true;
		*/
		
		// We will rewrite only if the two assignments assign to the same variable
		if(!(asgnThen.getLeftHandSide().subtreeMatch(new ASTMatcher(), asgnElse.getLeftHandSide())))
				return null;
		
		// Now create the new assignment with the conditional inside it
		ConditionalExpression newCondExp = ast.newConditionalExpression();
		newCondExp.setExpression((Expression) rewrite.createMoveTarget(node.getExpression()));
		newCondExp.setThenExpression((Expression) rewrite.createMoveTarget(asgnThen.getRightHandSide()));
		newCondExp.setElseExpression((Expression) rewrite.createMoveTarget(asgnElse.getRightHandSide()));
		
		Assignment newAsgn = ast.newAssignment();
		newAsgn.setOperator(Operator.ASSIGN);
		newAsgn.setRightHandSide(newCondExp);
		newAsgn.setLeftHandSide((Expression) rewrite.createMoveTarget(asgnThen.getLeftHandSide()));
		
		ExpressionStatement newnode = ast.newExpressionStatement(newAsgn);
		return newnode;
	}
	
}
