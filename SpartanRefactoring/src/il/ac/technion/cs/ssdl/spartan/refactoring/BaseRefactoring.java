package il.ac.technion.cs.ssdl.spartan.refactoring;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.TextFileChange;

public abstract class BaseRefactoring extends Refactoring {

	ITextSelection selection = null;
	ICompilationUnit compilationUnit = null;

	final Collection<TextFileChange> changes = new ArrayList<TextFileChange>();

	@Override
	public abstract String getName();

	protected abstract ASTRewrite createRewrite(CompilationUnit cu,
			SubProgressMonitor pm);

	protected boolean isTextSelected() {
		return selection != null && !selection.isEmpty()
				&& selection.getLength() != 0;
	}

	/**
	 * Determines if the node is outside of the selected text.
	 * 
	 * @return true if the node is not inside selection. If there is no
	 *         selection at all will return false.
	 */
	protected boolean isNodeOutsideSelection(ASTNode node) {
		if (!isTextSelected())
			return false;

		return (node.getStartPosition() > selection.getOffset()
				+ selection.getLength() || node.getStartPosition() < selection
				.getOffset());
	}

	@Override
	public RefactoringStatus checkInitialConditions(IProgressMonitor pm) {
		RefactoringStatus status = new RefactoringStatus();
		if (compilationUnit == null)
			status.merge(RefactoringStatus
					.createFatalErrorStatus("Nothing to refactor."));
		return status;
	}

	@Override
	public RefactoringStatus checkFinalConditions(IProgressMonitor pm)
			throws CoreException, OperationCanceledException {
		final RefactoringStatus status = new RefactoringStatus();
		
		// TODO: Catch exceptions and change status accordingly
		ArrayList<ICompilationUnit> units;
		pm.beginTask("Checking preconditions...", 2);
		
		if(isTextSelected()) {
			units = new ArrayList<ICompilationUnit>();
			units.add(compilationUnit);
		} else {
			units = getAllProjectCompilationUnits(new SubProgressMonitor(pm, 1, SubProgressMonitor.SUPPRESS_SUBTASK_LABEL));
		}
		
		scanCompilationUnits(units, new SubProgressMonitor(pm, 1, SubProgressMonitor.SUPPRESS_SUBTASK_LABEL));
		
		pm.done();
		return status;
	}

	/**
	 * Creates a change from each compilation unit and stores it in the changes array 
	 * @throws IllegalArgumentException 
	 * @throws JavaModelException 
	 */
	protected void scanCompilationUnits(ArrayList<ICompilationUnit> units, IProgressMonitor pm) throws JavaModelException, IllegalArgumentException {
		pm.beginTask("Iterating over gathered compilation units...", units.size());	
		for(ICompilationUnit u : units) {
			scanCompilationUnit(u, new SubProgressMonitor(pm, 1, SubProgressMonitor.SUPPRESS_SUBTASK_LABEL));
		}		
		pm.done();
	}

	/**
	 * @param u The compilation unit to scan
	 * @throws JavaModelException
	 */
	@SuppressWarnings("deprecation")
	protected void scanCompilationUnit(ICompilationUnit u, IProgressMonitor pm)
			throws JavaModelException {
		
		pm.beginTask("Creating change for a single compilation unit...", 2);	
		ASTParser parser = ASTParser.newParser(AST.JLS3);
		parser.setResolveBindings(false);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setSource(u);
		CompilationUnit cu = (CompilationUnit) parser
				.createAST(new SubProgressMonitor(pm, 1,
						SubProgressMonitor.SUPPRESS_SUBTASK_LABEL));

		TextFileChange textChange = new TextFileChange(
				u.getElementName(),
				(IFile) u.getResource());
		textChange.setTextType("java");
		textChange.setEdit(createRewrite(
				cu,
				new SubProgressMonitor(pm, 1,
						SubProgressMonitor.SUPPRESS_SUBTASK_LABEL))
				.rewriteAST());
		
		
		if (textChange.getEdit().getLength() != 0)
			changes.add(textChange);
		
		
		
		pm.done();
	}
	
	/**
	 * @param units
	 * @throws JavaModelException
	 */
	protected ArrayList<ICompilationUnit> getAllProjectCompilationUnits(IProgressMonitor pm) throws JavaModelException {	
		
		pm.beginTask("Gathering project information...", 1);
		
		ArrayList<ICompilationUnit> units = new ArrayList<ICompilationUnit>();
		IJavaProject proj = compilationUnit.getJavaProject();
		
		for (IPackageFragmentRoot r : proj.getPackageFragmentRoots()) {
			if (r.getKind() == IPackageFragmentRoot.K_SOURCE) {
				for (IJavaElement e : r.getChildren()) {
					if (e.getElementType() == IJavaElement.PACKAGE_FRAGMENT) {
						units.addAll(Arrays.asList(((IPackageFragment) e)
								.getCompilationUnits()));
					}
				}
			}
		}
		pm.done();
		return units;
	}

	

	@Override
	public Change createChange(IProgressMonitor pm) throws CoreException,
			OperationCanceledException {
		return new CompositeChange(getName(),
				changes.toArray(new Change[changes.size()]));
	}

	@SuppressWarnings("rawtypes")
	public RefactoringStatus initialize(Map fArguments) {
		RefactoringStatus status = new RefactoringStatus();
		return status;
	}

}
