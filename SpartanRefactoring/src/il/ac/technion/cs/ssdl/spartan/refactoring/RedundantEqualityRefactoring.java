package il.ac.technion.cs.ssdl.spartan.refactoring;


import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.BooleanLiteral;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.InfixExpression.Operator;
import org.eclipse.jdt.core.dom.PrefixExpression;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;

public class RedundantEqualityRefactoring extends BaseRefactoring {	
	@Override
	public String getName() {
		return "Remove Redundant Equality";
	}
	
	@Override
	protected ASTRewrite createRewrite(CompilationUnit cu, SubProgressMonitor pm) {
		pm.beginTask("Creating rewrite operation...", 1);
		
		final AST ast = cu.getAST();
		final ASTRewrite rewrite = ASTRewrite.create(ast);
		
		cu.accept(new ASTVisitor() {
			public boolean visit(InfixExpression node) {
				if (isNodeOutsideSelection(node))
					return true;
				
				if(node.getOperator() != Operator.EQUALS && node.getOperator() != Operator.NOT_EQUALS)
					return true;
				
				ASTNode nonLiteral = null;
				boolean literalValue;
				if (node.getRightOperand().getNodeType() == ASTNode.BOOLEAN_LITERAL) {
					nonLiteral   = rewrite.createMoveTarget(node.getLeftOperand());
					literalValue = ((BooleanLiteral)node.getRightOperand()).booleanValue();
				} else if (node.getLeftOperand().getNodeType() == ASTNode.BOOLEAN_LITERAL) {
					nonLiteral   = rewrite.createMoveTarget(node.getRightOperand());
					literalValue = ((BooleanLiteral)node.getLeftOperand()).booleanValue();	
				} else if(node.getRightOperand().getNodeType() == ASTNode.NULL_LITERAL) {
					nonLiteral   = rewrite.createMoveTarget(node.getLeftOperand());
					literalValue = false;
				} else if(node.getLeftOperand().getNodeType() == ASTNode.NULL_LITERAL) {
					nonLiteral   = rewrite.createMoveTarget(node.getRightOperand());
					literalValue = false;
				} else
					return true; 
				
				ASTNode newnode = null;
				if((literalValue && node.getOperator() == Operator.EQUALS) || 
				  (!literalValue && node.getOperator() == Operator.NOT_EQUALS)) {
					newnode = nonLiteral;
				} else {
					// No need for a ParenthesizedExpression here because the '==' operator has precedence over
					// all other binary boolean operators					
					newnode = ast.newPrefixExpression();

					// We can safely assume that the non literal part is an expression
					((PrefixExpression)newnode).setOperand((Expression)nonLiteral); 
					
					// TODO: Negate expression instead of just adding an "!" operator.
					((PrefixExpression)newnode).setOperator(PrefixExpression.Operator.NOT);
				}
				
				rewrite.replace(node, newnode, null);
				return true;
			}
		});
		pm.done();
		return rewrite;
	}	
}
