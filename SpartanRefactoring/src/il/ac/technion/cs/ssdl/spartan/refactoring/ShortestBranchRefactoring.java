package il.ac.technion.cs.ssdl.spartan.refactoring;


import java.util.concurrent.atomic.AtomicInteger;

import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ConditionalExpression;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.ParenthesizedExpression;
import org.eclipse.jdt.core.dom.PrefixExpression;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;

public class ShortestBranchRefactoring extends BaseRefactoring {	
	@Override
	public String getName() {
		return "Shortest Conditional Branch First";
	}
	
	/**
	 * Count number of nodes in the tree of which node is root.
	 * @param node The node.
	 * @return Number of ast nodes under the node.
	 */
	private int countNodes(ASTNode node) {
		final AtomicInteger c = new AtomicInteger(0);
		node.accept(new ASTVisitor() {
			public void preVisit(ASTNode node) {
				c.incrementAndGet();
			}
		});
		
		return c.get();		
	}
	
	@Override
	protected ASTRewrite createRewrite(CompilationUnit cu, SubProgressMonitor pm) {
		pm.beginTask("Creating rewrite operation...", 1);
		
		final AST ast = cu.getAST();
		final ASTRewrite rewrite = ASTRewrite.create(ast);
		
		cu.accept(new ASTVisitor() {
			public boolean visit(IfStatement node) {
				if (isNodeOutsideSelection(node))
					return true;
				
				if (node.getElseStatement() == null)
					return true;
				
				int thenCount = countNodes(node.getThenStatement());
				int elseCount = countNodes(node.getElseStatement());
				
				if(thenCount <= elseCount)
					return true;
					
				IfStatement newnode = ast.newIfStatement();	
				PrefixExpression neg = negateExpression(ast, rewrite, node.getExpression());	
				newnode.setExpression(neg);
					
				newnode.setThenStatement((org.eclipse.jdt.core.dom.Statement) rewrite.createMoveTarget(node.getElseStatement()));
				newnode.setElseStatement((org.eclipse.jdt.core.dom.Statement) rewrite.createMoveTarget(node.getThenStatement()));	
				
				rewrite.replace(node, newnode, null);						
				return true;
			}
			
			public boolean visit(ConditionalExpression node) {
				if (isNodeOutsideSelection(node))
					return true;
				
				if (node.getElseExpression() == null)
					return true;
				
				if(node.getThenExpression().getLength() <= node.getElseExpression().getLength())
					return true;
				
				ConditionalExpression newnode = ast.newConditionalExpression();	
				PrefixExpression neg = negateExpression(ast, rewrite, node.getExpression());	
				newnode.setExpression(neg);
					
				newnode.setThenExpression((Expression) rewrite.createMoveTarget(node.getElseExpression()));
				newnode.setElseExpression((Expression) rewrite.createMoveTarget(node.getThenExpression()));	
				
				rewrite.replace(node, newnode, null);						
				
				return true;
			}
		});
		pm.done();
		return rewrite;
	}

	/**
	 * @return Returns a prefix expression that is the negation of the provided expression.
	 */
	private PrefixExpression negateExpression(final AST ast,
			final ASTRewrite rewrite, Expression exp) {
		ParenthesizedExpression paren = ast.newParenthesizedExpression();
		paren.setExpression((Expression) rewrite.createCopyTarget(exp));
		PrefixExpression neg = ast.newPrefixExpression();
		neg.setOperand(paren);
		neg.setOperator(PrefixExpression.Operator.NOT);
		return neg;
	}	
}
