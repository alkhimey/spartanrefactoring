# README #

Spartanization is a refactoring of a code which aims to aggressively reduce the number of lines, tokens, variables, functions and other entities. The rational behind this methodology is to increase simplicity and maintainability of the code.

The following article describes the full theory with examples.

This plug-in makes it possible to perform some of the spartanizations automatically. The following refactorings will be added into the Eclipse's "Refactor" menu:

1. Convert Conditional to Ternary - when possible, this refactoring will convert short if-else statements into ternary statements thus reducing the number of lines.
2. Eliminate Redundant Equalities - will remove true or false from expressions which explicitly compare a Boolean with true or with false.
3. Shortest Conditional Branch First - will switch between if and else branches of if-else statements to make sure that the shortest branch is always first.

The update site of this plug-in is [http://update.nihamkin.com/spartan](http://update.nihamkin.com/spartan). It is also available for install from eclipse marketplace [http://marketplace.eclipse.org/content/spartan-refactoring](http://marketplace.eclipse.org/content/spartan-refactoring)

![screenshot](http://marketplace.eclipse.org/sites/default/files/spartanization_refactoring.png)

### Contribution guidelines ###

This is a small project so there are no specific contribution guidelines. If you have anything to contribute such as code, bug reports or feature ideas, I would be very happy to hear from you.

![email3.gif](https://bitbucket.org/repo/Egbzko/images/1803176418-email3.gif)